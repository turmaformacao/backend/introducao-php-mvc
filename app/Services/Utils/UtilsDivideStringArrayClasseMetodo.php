<?php
namespace App\Services\Utils;

class UtilsDivideStringArrayClasseMetodo
{
    static public function divideStringArray(String $string)
    {
        $tmpArray = explode('@', $string);
        return [
                'classe' => $tmpArray[0],
                'metodo' => $tmpArray[1]
                ];
                
    }
}