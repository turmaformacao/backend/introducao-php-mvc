<?php
namespace App\Services\HttpClient;

use GuzzleHttp\Client;

class ServiceHttpClient
{
    function __construct()
    {
        $this->client = new Client();
    }

    public function get($url)
    {
        return  $this->client->request('GET', $url)->getBody()->getContents();
    }

    public function post($url, Array $data)
    {
        return $this->client->request('POST', $url, $data)->getBody()->getContents();
    }

}
