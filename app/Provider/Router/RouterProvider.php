<?php
namespace App\Provider\Router;

use Router\Web;
use App\Services\Utils\UtilsDivideStringArrayClasseMetodo;

class RouterProvider extends Web
{

    private $rota_atual;
    
    function __construct()
    {
      $this->rota_atual = $_SERVER['REQUEST_URI'];
    }

    public function start()
    {
        foreach($this->route as $rota => $classe_controller) {
            if ($rota == $this->rota_atual) {
                
                $classe_metodo = UtilsDivideStringArrayClasseMetodo::divideStringArray($classe_controller);
                return $this->constroiInstanciaClasse($classe_metodo);
                
            }
        }

        echo 'Rota não encontrada!';
    }

    private function constroiInstanciaClasse(Array $classe_metodo): void {
        $classe = 'App\\Http\\Controller\\' . $classe_metodo['classe'];
        $instancia = new $classe;
        $metodo = $classe_metodo['metodo'];
        echo $instancia->$metodo();
    }
}