<?php
namespace App\Http\Controller;

use App\Services\HttpClient\ServiceHttpClient as HttpClient;

class ConsultarCepController
{

    function __construct()
    {
        $this->httpClient = new HttpClient();
    }


    public function consultar()
    {
        $data = ['nome' => 'Turma', 'email' => 'email@email'];
        $url = 'https://viacep.com.br/ws/'. $cep .'/json/';

        return $this->httpClient->post($url, $data);
    } 


}