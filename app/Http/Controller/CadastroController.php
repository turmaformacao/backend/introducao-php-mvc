<?php
namespace App\Http\Controller;

class CadastroController
{
    public $dados_view = ['nome' => 'Marcelo', 'email' => 'email@email'];

    public function cadastrar()
    {
        return view('cadastro.view.php', $this->dados_view);
    }

}