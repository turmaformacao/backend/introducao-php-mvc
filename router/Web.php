<?php
namespace Router;

class Web
{

     public $route = [
        '/' => 'HomeController@index',
        '/home' => 'HomeController@index',
        '/cadastro' => 'CadastroController@cadastrar',
        '/consultarcep' => 'ConsultarCepController@consultar'
    ];

}