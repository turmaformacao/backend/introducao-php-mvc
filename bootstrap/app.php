<?php

include_once __DIR__ . '/global.php';

$route = new App\Provider\Router\RouterProvider();
$route->start();